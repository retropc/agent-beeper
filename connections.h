#ifndef CONNECTIONS_H
#define CONNECTIONS_H

#include <sys/epoll.h>

#define TYPE_SOCKET 0
#define TYPE_AGENT 1

struct connection;

struct socket_connection {
  int type;
  int fd;
  struct connection *conn;
  struct epoll_event epoll;
};

struct connection {
  struct socket_connection socket, agent;
  int connected;
  pid_t pid;
  int ep_fd;
};

struct connection *new_connection(pid_t pid, int ep_fd, int socket_fd, int agent_fd, int connected);
void close_connection(struct connection *conn);
struct socket_connection *lookup_socket(int fd);

#endif

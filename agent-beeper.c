#define _POSIX_C_SOURCE 199309L
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/epoll.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include "connections.h"

static char **program_args;
static struct sockaddr_un agent_addr;
static int ep_fd, l_fd;

static int setup_sockaddr_un(struct sockaddr_un *addr, const char *path) {
  memset(addr, 0, sizeof(struct sockaddr_un));

  addr->sun_family = AF_UNIX;
  strncpy(addr->sun_path, path, sizeof(addr->sun_path) - 1);
  addr->sun_path[sizeof(addr->sun_path) - 1] = '\0';

  if (strcmp(addr->sun_path, path)) {
    fprintf(stderr, "path too long\n");
    return 0;
  }

  return 1;
}

static int create_listener(const char *path) {
  struct sockaddr_un addr;
  if (!setup_sockaddr_un(&addr, path)) {
    return -1;
  }

  int fd = socket(AF_UNIX, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0);
  if (fd == -1) {
    perror("socket");
    return -1;
  }

  if (fchmod(fd, 0700) == -1) {
    perror("fchmod");
    close(fd);
    return -1;
  }

  unlink(addr.sun_path);
  if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
    perror("bind");
    close(fd);
    return -1;
  }

  if (listen(fd, 5) == -1) {
    perror("listen");
    close(fd);
    return -1;
  }

  return fd;
}

static int exec_beep(struct connection *c) {
  int devnull = open("/dev/null", O_RDWR|O_CLOEXEC);
  if (devnull == -1) {
    perror("open");
    return 1;
  }

  int r = dup2(devnull, 0);
  if (r == -1) {
    perror("dup2");
    close(devnull);
    return 1;
  }
  close(devnull);

  char pidstr[50];
  snprintf(pidstr, sizeof(pidstr), "%d", c->pid);
  setenv("SSH_CONN_PID", pidstr, 1);

  /* argv[argc] == NULL by ansi standard */
  execvp(program_args[0], program_args);

  perror("execvp");
  return 1;
}

static int beep(struct connection *c) {
  pid_t p = fork();
  if (p == -1) {
    perror("fork");
    return 0;
  }
  if (p != 0) {
    /* parent */
    return 1;
  }

  /* child */
  _exit(exec_beep(c));
}

static int connect_agent(int *fd_out) {
  int fd = socket(AF_UNIX, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0);
  if (fd == -1) {
    perror("socket");
    return -1;
  }

  if (connect(fd, (struct sockaddr *)&agent_addr, sizeof(agent_addr)) == -1) {
    if (errno != EINPROGRESS) {
      perror("connect");
      close(fd);
      return -1;
    }
    *fd_out = fd;
    return 0;
  }

  *fd_out = fd;
  return 1;
}

static int pump(int src, int dst) {
  char buf[8192];
  ssize_t r = recv(src, buf, sizeof(buf), 0);
  if (r == -1) {
    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      return 1;
    }
    perror("recv");
    return -1;
  }
  if (r == 0) {
    fprintf(stderr, "recv: EOF\n");
    return 0;
  }

  ssize_t pos = 0;
  while (r > 0) {
    ssize_t r2 = send(dst, buf + pos, r, MSG_NOSIGNAL);
    if (r2 == -1) {
      /* no buffer */
      perror("send");
      return -1;
    }
    if (r2 == 0) {
      fprintf(stderr, "send: EOF\n");
      return 0;
    }
    pos+=r2;
    r-=r2;
  }

  return 1;
}

static void finish_agent_connection(struct connection *c) {
  int error;
  socklen_t len = sizeof(error);
  if (getsockopt(c->agent.fd, SOL_SOCKET, SO_ERROR, &error, &len) < -1) {
    perror("getsockopt");
    close_connection(c);
    return;
  }

  if (error != 0) {
    errno = error;
    perror("connect");
    close_connection(c);
    return;
  }

  c->agent.epoll.events = EPOLLIN;
  if (epoll_ctl(ep_fd, EPOLL_CTL_MOD, c->agent.fd, &c->agent.epoll) == -1) {
    perror("epoll_ctl");
    close_connection(c);
    return;
  }

  c->socket.epoll.events = EPOLLIN;
  if (epoll_ctl(ep_fd, EPOLL_CTL_MOD, c->socket.fd, &c->socket.epoll) == -1) {
    perror("epoll_ctl");
    close_connection(c);
    return;
  }

  c->connected = 1;
}

static void handle_socket(struct socket_connection *sc) {
  struct connection *c = sc->conn;

  int fd = sc->fd;
  int other_fd;
  if (sc->type == TYPE_AGENT) {
    if (!c->connected) {
      finish_agent_connection(c);
      return;
    }
    other_fd = c->socket.fd;
  } else {
    other_fd = c->agent.fd;
  }

  int r = pump(fd, other_fd);
  if (r <= 0) {
    close_connection(c);
    return;
  }
}

static void handle_listener(void) {
  int s2 = accept4(l_fd, NULL, NULL, SOCK_NONBLOCK|SOCK_CLOEXEC);
  if (s2 == -1) {
    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      return;
    }
    perror("accept");
    return;
  }

  pid_t pid;
  {
    struct ucred ucred;
    unsigned int len = sizeof(struct ucred);

    if (getsockopt(s2, SOL_SOCKET, SO_PEERCRED, &ucred, &len) == -1) {
      fprintf(stderr, "unable to lookup peer credentials\n");
      close(s2);
      return;
    }

    if (ucred.uid != getuid()) {
      fprintf(stderr, "denied: connecting process owned by other user, uid: %d\n", ucred.uid);
      close(s2);
      return;
    }

    pid = ucred.pid;
  }

  int s3;
  int r = connect_agent(&s3);
  if (r == -1) {
    close(s2);
    return;
  }

  struct connection *c = new_connection(pid, ep_fd, s2, s3, r != 0);
  if (!c) {
    close(s2);
    close(s3);
    return;
  }

  if (!beep(c)) {
    close_connection(c);
    return;
  }
}

int main(int argc, char **argv) {
  if (argc < 4) {
    fprintf(stderr, "args: %s [listen path] [agent path] [program to run] ?arg1? ?arg2? ... ?argn?\n", argv[0]);
    return 1;
  }

  const char *listen_path = argv[1];
  const char *agent_path = argv[2];
  program_args = &argv[3];

  struct sigaction sa = { 0 };
  sa.sa_handler = SIG_IGN;
  sa.sa_flags = SA_NOCLDSTOP|SA_NOCLDWAIT|SA_RESTART;
  if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    perror("sigaction");
    return 1;
  }

  if (!setup_sockaddr_un(&agent_addr, agent_path)) {
    return 1;
  }

  ep_fd = epoll_create1(EPOLL_CLOEXEC);
  if (ep_fd == -1) {
    perror("epoll_create1");
    return 1;
  }

  l_fd = create_listener(listen_path);
  if (l_fd == -1) {
    return 1;
  }

  struct epoll_event l_e = { .events = EPOLLIN, .data.fd = -1 };
  if (epoll_ctl(ep_fd, EPOLL_CTL_ADD, l_fd, &l_e) == -1) {
    perror("epoll_ctl");
    return 1;
  }

  for (;;) {
    struct epoll_event events[50];
    int count = epoll_wait(ep_fd, events, 50, -1);
    if (count == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("epoll_wait");
      break;
    }

    for(int i=0;i<count;i++) {
      struct epoll_event *e = &events[i];
      if (e->data.fd == -1) {
        handle_listener();
      }
    }

    for(int i=0;i<count;i++) {
      struct epoll_event *e = &events[i];
      if (e->data.fd != -1) {
        struct socket_connection *c = lookup_socket(e->data.fd);
        if (c == NULL) {
          /* we've just closed it */
        } else {
          handle_socket(c);
        }
      }
    }
  }

  return 1;
}

CFLAGS=-std=c99 -pedantic -Wall -Werror -g

.PHONY: clean

agent-beeper: agent-beeper.o connections.o

agent-beeper.o: connections.h

connections.o: connections.h

clean:
	rm -f *.o agent-beeper

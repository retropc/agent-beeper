#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "connections.h"

static struct connection **fds;
static int fds_size;

static int ensure_capacity(int pos) {
  if (pos < fds_size) {
    return 1;
  }

  pos+=100;

  struct connection **new_fds = realloc(fds, sizeof(struct connection *) * pos);
  if (new_fds == NULL) {
    perror("realloc");
    return 0;
  }

  memset(new_fds + sizeof(struct connection *) * fds_size, 0, sizeof(struct connection *) * (pos - fds_size));
  fds = new_fds;
  fds_size = pos;
  return 1;
}

struct connection *new_connection(pid_t pid, int ep_fd, int socket_fd, int agent_fd, int connected) {
  if (!ensure_capacity(socket_fd > agent_fd ? socket_fd : agent_fd)) {
    return NULL;
  }

  struct connection *c = malloc(sizeof(struct connection));
  if (c == NULL) {
    perror("malloc");
    return NULL;
  }

  c->ep_fd = ep_fd;
  c->pid = pid;
  c->connected = connected;

  c->socket.type = TYPE_SOCKET;
  c->socket.fd = socket_fd;
  c->socket.conn = c;
  c->socket.epoll = (struct epoll_event){ .data.fd = socket_fd };

  c->agent.type = TYPE_AGENT;
  c->agent.fd = agent_fd;
  c->agent.conn = c;
  c->agent.epoll = (struct epoll_event){ .data.fd = agent_fd };

  if (connected) {
    c->agent.epoll.events = EPOLLIN;
    c->socket.epoll.events = EPOLLIN;
  } else {
    c->agent.epoll.events = EPOLLOUT;
    c->socket.epoll.events = 0;
  }

  if (epoll_ctl(ep_fd, EPOLL_CTL_ADD, c->socket.fd, &c->socket.epoll) == -1) {
    perror("epoll_ctl");
    free(c);
    return NULL;
  }

  if (epoll_ctl(ep_fd, EPOLL_CTL_ADD, c->agent.fd, &c->agent.epoll) == -1) {
    perror("epoll_ctl");

    epoll_ctl(ep_fd, EPOLL_CTL_DEL, c->socket.fd, &c->socket.epoll);
    free(c);
    return NULL;
  }

  fds[socket_fd] = c;
  fds[agent_fd] = c;

  return c;
}

struct socket_connection *lookup_socket(int fd) {
  assert(fd >= 0);
  assert(fd < fds_size);

  struct connection *c = fds[fd];
  if (c == NULL) {
    return NULL;
  }

  if (c->socket.fd == fd) {
    return &c->socket;
  }
  if (c->agent.fd == fd) {
    return &c->agent;
  }

  assert(0);
  return NULL;
}

void close_connection(struct connection *c) {
  assert(c->agent.fd >= 0);
  assert(c->socket.fd >= 0);

  /* shouldn't be necessary as all our FDs are CLOEXEC... but hey */
  epoll_ctl(c->ep_fd, EPOLL_CTL_DEL, c->agent.fd, &c->agent.epoll);
  epoll_ctl(c->ep_fd, EPOLL_CTL_DEL, c->socket.fd, &c->socket.epoll);

  close(c->agent.fd);
  close(c->socket.fd);

  fds[c->agent.fd] = NULL;
  fds[c->socket.fd] = NULL;

  c->agent.fd = -1;
  c->socket.fd = -1;

  free(c);
}
